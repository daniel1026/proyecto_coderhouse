CREATE SCHEMA IF NOT EXISTS PROYECTO;
USE PROYECTO;

DROP TABLE IF EXISTS FACTURAS;
DROP TABLE IF EXISTS CLIENTES;
DROP TABLE IF EXISTS PRODUCTOS;

-- Base de datos de los clientes --
CREATE TABLE CLIENTES(
id_Cliente INT AUTO_INCREMENT,
nombre VARCHAR(150),
apellido VARCHAR(150),
dni INT,
primary key(id_Cliente)
);

insert into CLIENTES
(nombre, apellido, dni)  
value 
('Daniel', 'Herrera', 123456789);

SELECT * FROM CLIENTES;

-- Base de datos de los productos --
CREATE TABLE PRODUCTOS(
id_Producto INT AUTO_INCREMENT,
Codigo INT,
Descripcion VARCHAR(150),
Precio FLOAT,
stock INT,
primary key(id_Producto)
);

INSERT INTO PRODUCTOS
(Codigo, Descripcion, Precio, stock)
VALUES
(10, 'balon', 100000.0, 15);

SELECT * FROM PRODUCTOS;

-- Base de datos de las ordenes --
CREATE TABLE FACTURAS(
id_Factura INT AUTO_INCREMENT,
fecha DATE,
serie_factura INT, -- Numero de serie unico, donde se tiene en cuenta el año y una serie numerica
id_Cliente INT,
id_Producto INT,
cantidad_Comprada INT,
precio_Total FLOAT,
primary key(Id_Factura),
CONSTRAINT FK_Id_Cliente FOREIGN KEY (id_Cliente) REFERENCES CLIENTES(id_Cliente),
CONSTRAINT FK_Id_Producto FOREIGN KEY (id_Producto) REFERENCES PRODUCTOS(id_Producto)
);

INSERT INTO FACTURAS
(Fecha, serie_factura, Id_Cliente, Id_Producto, Cantidad_Comprada, Precio_Total)
VALUES
('2022-03-21', 202201, 1, 1, 2, 200000.0 );

SELECT * FROM FACTURAS;