package com.proyecto.coderhouse.facturacion.controller;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaRequest;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaResponse;
import com.proyecto.coderhouse.facturacion.service.factura.IFacturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/factura")
public class FacturaController {

    @Autowired
    private IFacturaService iFacturaService;

    @GetMapping("/llamar")
    public List<FacturaEntity> obtenerFacturas() {
        return iFacturaService.buscarTodos();
    }

    @GetMapping("/{dni}")
    public ResponseEntity<FacturaResponse> buscarFacturaPorSerie(@PathVariable Long serie) {
        return ResponseEntity.ok(iFacturaService.buscarPorSerie(serie));
    }

    @PostMapping("/crear")
    public ResponseEntity<FacturaResponse> crearFactura(@Valid @RequestBody FacturaRequest factura) throws ApiException {
        return ResponseEntity.ok(iFacturaService.crearFactura(factura));
    }

    @PostMapping("/actualizar")
    public ResponseEntity<FacturaResponse> actualizarFactura(@Valid @RequestBody FacturaRequest factura) throws ApiException {
        return ResponseEntity.ok(iFacturaService.actualizarFactura(factura));
    }

    @PostMapping("/eliminar")
    public void eliminar(@Valid @RequestBody FacturaRequest factura) throws ApiException {
        iFacturaService.eliminarFactura(factura);
    }

}
