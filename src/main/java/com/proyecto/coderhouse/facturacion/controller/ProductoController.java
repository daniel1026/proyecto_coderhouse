package com.proyecto.coderhouse.facturacion.controller;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoResponse;
import com.proyecto.coderhouse.facturacion.service.producto.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private IProductoService iProductoService;

    @GetMapping("/llamar")
    public List<ProductoEntity> obtenerProductos() {
        return iProductoService.buscarTodos();
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<ProductoResponse> buscarClientePorCodigo(@PathVariable Long codigo) {
        return ResponseEntity.ok(iProductoService.buscarPorCodigo(codigo));
    }

    @PostMapping("/crear")
    public ResponseEntity<ProductoResponse> crearProducto(@Valid @RequestBody ProductoRequest producto) throws ApiException {
        return ResponseEntity.ok(iProductoService.crearProducto(producto));
    }

    @PostMapping("/actualizar")
    public ResponseEntity<ProductoResponse> actualizarProducto(@Valid @RequestBody ProductoRequest producto) throws ApiException {
        return ResponseEntity.ok(iProductoService.actualizarProducto(producto));
    }

    @PostMapping("/eliminar")
    public void eliminar(@Valid @RequestBody ProductoRequest producto) throws ApiException {
        iProductoService.eliminarProducto(producto);
    }

}
