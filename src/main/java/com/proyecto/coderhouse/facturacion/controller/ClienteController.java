package com.proyecto.coderhouse.facturacion.controller;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteResponse;
import com.proyecto.coderhouse.facturacion.service.cliente.IClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private IClientesService iClientesService;

   @GetMapping("/llamar")
   public List<ClienteEntity> obtenerClientes() {
       return iClientesService.buscarTodos();
   }

    @GetMapping("/{dni}")
    public ResponseEntity<ClienteResponse> buscarClientePorDni(@PathVariable Long dni) {
        return ResponseEntity.ok(iClientesService.buscarPorDni(dni));
    }

    @PostMapping("/crear")
    public ResponseEntity<ClienteResponse> crearCliente(@Valid @RequestBody ClienteRequest cliente) throws ApiException {
        return ResponseEntity.ok(iClientesService.crearCliente(cliente));
    }

    @PostMapping("/actualizar")
    public ResponseEntity<ClienteResponse> actualizarCliente(@Valid @RequestBody ClienteRequest cliente) throws ApiException {
        return ResponseEntity.ok(iClientesService.actualizarCliente(cliente));
    }

    @PostMapping("/eliminar")
    public void eliminar(@Valid @RequestBody ClienteRequest cliente) throws ApiException {
        iClientesService.eliminarCliente(cliente);
    }

}
