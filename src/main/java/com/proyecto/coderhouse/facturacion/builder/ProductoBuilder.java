package com.proyecto.coderhouse.facturacion.builder;


import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoResponse;

import java.util.Objects;

public class ProductoBuilder {

    public static ProductoResponse entityToResponse(ProductoEntity producto) {
        if (Objects.nonNull(producto)) {
            return ProductoResponse.builder()
                    .idProducto(producto.getIdProducto())
                    .codigo(producto.getCodigo())
                    .descripcion(producto.getDescripcion())
                    .precio(producto.getPrecio())
                    .stock(producto.getStock())
                    .build();
        }
        return null;
    }

    public static ProductoEntity requestToEntity(ProductoRequest producto) {
        if (Objects.nonNull(producto)) {
            return ProductoEntity.builder()
                    .idProducto(producto.getIdProducto())
                    .codigo(producto.getCodigo())
                    .descripcion(producto.getDescripcion())
                    .precio(producto.getPrecio())
                    .stock(producto.getStock())
                    .build();
        }
        return null;
    }
}
