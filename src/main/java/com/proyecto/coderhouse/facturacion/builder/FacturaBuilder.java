package com.proyecto.coderhouse.facturacion.builder;


import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaRequest;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaResponse;

import java.util.Objects;

public class FacturaBuilder {

    public static FacturaResponse entityToResponse(FacturaEntity factura) {
        if (Objects.nonNull(factura)) {
            return FacturaResponse.builder()
                    .idFactura(factura.getIdFactura())
                    .fecha(factura.getFecha())
                    .serieFactura(factura.getSerieFactura())
                    .cantidadComprada(factura.getCantidadComprada())
                    .precioTotal(factura.getPrecioTotal())
                    .build();
        }
        return null;
    }

    public static FacturaEntity requestToEntity(FacturaRequest factura) {
        if (Objects.nonNull(factura)) {
            return FacturaEntity.builder()
                    .idFactura(factura.getIdFactura())
                    .fecha(factura.getFecha())
                    .serieFactura(factura.getSerieFactura())
                    .cantidadComprada(factura.getCantidadComprada())
                    .precioTotal(factura.getPrecioTotal())
                    .build();
        }
        return null;
    }
}
