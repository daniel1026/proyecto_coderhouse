package com.proyecto.coderhouse.facturacion.builder;

import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteResponse;

import java.util.Objects;

public class ClienteBuilder {

    public static ClienteResponse entityToResponse(ClienteEntity cliente) {
        if (Objects.nonNull(cliente)) {
            return ClienteResponse.builder()
                    .idCliente(cliente.getIdCliente())
                    .nombre(cliente.getNombre())
                    .apellido(cliente.getApellido())
                    .dni(cliente.getDni())
                    .build();
        }
        return null;
    }

    public static ClienteEntity requestToEntity(ClienteRequest cliente) {
        if (Objects.nonNull(cliente)) {
            return ClienteEntity.builder()
                    .idCliente(cliente.getIdCliente())
                    .nombre(cliente.getNombre())
                    .apellido(cliente.getApellido())
                    .dni(cliente.getDni())
                    .build();
        }
        return null;
    }
}
