package com.proyecto.coderhouse.facturacion.repository;

import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteRepository extends JpaRepository<ClienteEntity, Long> {
}
