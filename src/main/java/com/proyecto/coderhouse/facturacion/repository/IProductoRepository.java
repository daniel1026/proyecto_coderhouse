package com.proyecto.coderhouse.facturacion.repository;

import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductoRepository extends JpaRepository<ProductoEntity, Long> {
}
