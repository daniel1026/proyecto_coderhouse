package com.proyecto.coderhouse.facturacion.repository;


import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFacturaRepository extends JpaRepository<FacturaEntity, Long> {
}
