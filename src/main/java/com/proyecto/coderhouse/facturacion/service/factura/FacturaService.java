package com.proyecto.coderhouse.facturacion.service.factura;


import com.proyecto.coderhouse.facturacion.builder.FacturaBuilder;
import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaRequest;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaResponse;
import com.proyecto.coderhouse.facturacion.repository.IFacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacturaService implements IFacturaService{

    @Autowired
    private IFacturaRepository iFacturaRepository;


    @Override
    public List<FacturaEntity> buscarTodos() {
        return iFacturaRepository.findAll();
    }

    @Override
    public FacturaResponse buscarPorSerie(Long serie) {
        FacturaEntity factura = iFacturaRepository.findById(serie).orElse(null);
        return FacturaBuilder.entityToResponse(factura);
    }

    @Override
    public FacturaResponse crearFactura(FacturaRequest factura) throws ApiException {
        try {
            if (buscarPorSerie(factura.getSerieFactura()) == null) {
                FacturaEntity entity = iFacturaRepository.save(FacturaBuilder.requestToEntity(factura));
                return FacturaBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("La factura ya existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public FacturaResponse actualizarFactura(FacturaRequest factura) throws ApiException {
        try {
            if (buscarPorSerie(factura.getSerieFactura()) != null) {
                FacturaEntity entity = iFacturaRepository.save(FacturaBuilder.requestToEntity(factura));
                return FacturaBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("La factura no existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public void eliminarFactura(FacturaRequest factura) throws ApiException {

        if(buscarPorSerie(factura.getSerieFactura()) != null){
            FacturaEntity FacturaEliminar = FacturaBuilder.requestToEntity(factura);
            iFacturaRepository.delete(FacturaEliminar);
            throw new ApiException("Factura Eliminada");
        }
        else {
            throw new ApiException("La factura no existe");
        }

    }
}
