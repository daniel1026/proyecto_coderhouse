package com.proyecto.coderhouse.facturacion.service.producto;

import com.proyecto.coderhouse.facturacion.builder.ClienteBuilder;
import com.proyecto.coderhouse.facturacion.builder.ProductoBuilder;
import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoResponse;
import com.proyecto.coderhouse.facturacion.repository.IProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductoService implements IProductoService{

    @Autowired
    private IProductoRepository iProductoRepository;

    @Override
    public List<ProductoEntity> buscarTodos() {
        return iProductoRepository.findAll();
    }

    @Override
    public ProductoResponse buscarPorCodigo(Long codigo) {
        ProductoEntity producto = iProductoRepository.findById(codigo).orElse(null);
        return ProductoBuilder.entityToResponse(producto);
    }

    @Override
    public ProductoResponse crearProducto(ProductoRequest producto) throws ApiException {
        try {
            if (buscarPorCodigo(producto.getCodigo()) == null) {
                ProductoEntity entity = iProductoRepository.save(ProductoBuilder.requestToEntity(producto));
                return ProductoBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("El producto ya existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public ProductoResponse actualizarProducto(ProductoRequest producto) throws ApiException {
        try {
            if (buscarPorCodigo(producto.getCodigo()) != null) {
                ProductoEntity entity = iProductoRepository.save(ProductoBuilder.requestToEntity(producto));
                return ProductoBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("El producto no existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public void eliminarProducto(ProductoRequest producto) throws ApiException {
        if (buscarPorCodigo(producto.getCodigo()) != null) {
            ProductoEntity productoEliminar = ProductoBuilder.requestToEntity(producto);
            iProductoRepository.delete(productoEliminar);
            throw new ApiException("Cliente Eliminado");
        } else {
            throw new ApiException("El codigo no existe");
        }
    }
}
