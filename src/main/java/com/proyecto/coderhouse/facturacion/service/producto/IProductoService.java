package com.proyecto.coderhouse.facturacion.service.producto;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoResponse;

import java.util.List;

public interface IProductoService {

    List<ProductoEntity> buscarTodos();

    ProductoResponse buscarPorCodigo(Long codigo);

    ProductoResponse crearProducto(ProductoRequest producto) throws ApiException;

    ProductoResponse actualizarProducto(ProductoRequest producto) throws ApiException;

    void  eliminarProducto(ProductoRequest producto) throws ApiException;


}
