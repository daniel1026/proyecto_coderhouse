package com.proyecto.coderhouse.facturacion.service.cliente;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteResponse;


import java.util.List;

public interface IClientesService {

    List<ClienteEntity> buscarTodos();

    ClienteResponse buscarPorDni(Long dni);

    ClienteResponse crearCliente(ClienteRequest cliente) throws ApiException;

    ClienteResponse actualizarCliente(ClienteRequest cliente) throws ApiException;

    void eliminarCliente(ClienteRequest cliente) throws ApiException;
}
