package com.proyecto.coderhouse.facturacion.service.factura;

import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaRequest;
import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaResponse;

import java.util.List;

public interface IFacturaService {

    List<FacturaEntity> buscarTodos();

    FacturaResponse buscarPorSerie(Long serie);

    FacturaResponse crearFactura(FacturaRequest factura) throws ApiException;

    FacturaResponse actualizarFactura(FacturaRequest factura) throws ApiException;

    void eliminarFactura(FacturaRequest factura) throws ApiException;

}
