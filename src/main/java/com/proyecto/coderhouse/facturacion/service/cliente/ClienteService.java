package com.proyecto.coderhouse.facturacion.service.cliente;


import com.proyecto.coderhouse.facturacion.builder.ClienteBuilder;
import com.proyecto.coderhouse.facturacion.handle.ApiException;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteResponse;
import com.proyecto.coderhouse.facturacion.repository.IClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClienteService implements IClientesService{

    @Autowired
    private IClienteRepository iClienteRepository;

    @Override
    public List<ClienteEntity> buscarTodos() {
        return iClienteRepository.findAll();
    }

    @Override
    public ClienteResponse buscarPorDni(Long dni) {
        ClienteEntity cliente = iClienteRepository.findById(dni).orElse(null);
        return ClienteBuilder.entityToResponse(cliente);
    }

    @Override
    public ClienteResponse crearCliente(ClienteRequest cliente) throws ApiException {
        try {
            if (buscarPorDni(cliente.getDni()) == null) {
                ClienteEntity entity = iClienteRepository.save(ClienteBuilder.requestToEntity(cliente));
                return ClienteBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("El cliente ya existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public ClienteResponse actualizarCliente(ClienteRequest cliente) throws ApiException {
        try {
            if (buscarPorDni(cliente.getDni()) != null) {
                ClienteEntity entity = iClienteRepository.save(ClienteBuilder.requestToEntity(cliente));
                return ClienteBuilder.entityToResponse(entity);
            } else {
                throw new ApiException("El cliente no existe");
            }
        } catch (Exception e) {
            throw new ApiException(e.getMessage());
        }
    }

    @Override
    public void eliminarCliente(ClienteRequest cliente) throws ApiException {

        if(buscarPorDni(cliente.getDni()) != null){
            ClienteEntity clienteEliminar = ClienteBuilder.requestToEntity(cliente);
            iClienteRepository.delete(clienteEliminar);
            throw new ApiException("Cliente Eliminado");
        }
        else {
            throw new ApiException("El Cliente no existe");
        }

    }


}
