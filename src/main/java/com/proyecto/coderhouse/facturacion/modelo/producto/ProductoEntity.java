package com.proyecto.coderhouse.facturacion.modelo.producto;

import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "PRODUCTOS")
public class ProductoEntity {


    @Column(name = "ID_PRODUCTO")
    private String idProducto;

    @Column(name = "CODIGO")
    @Id
    private Long codigo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "PRECIO")
    private Float precio;

    @Column(name = "STOCK")
    private Integer stock;

}
