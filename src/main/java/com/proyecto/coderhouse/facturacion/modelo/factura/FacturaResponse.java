package com.proyecto.coderhouse.facturacion.modelo.factura;

import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import lombok.Builder;
import lombok.Data;
import java.sql.Date;

@Data
@Builder
public class FacturaResponse {


    private String idFactura;
    private Date fecha;
    private Long serieFactura;
    private Long cantidadComprada;
    private int precioTotal;

}
