package com.proyecto.coderhouse.facturacion.modelo.factura;

import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteEntity;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "FACTURAS")
public class FacturaEntity {

    @Column(name = "ID_FACTURA")
    private String idFactura;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "SERIE_FACTURA")
    @Id
    private Long serieFactura;

    @Column(name = "CANTIDAD_COMPRADA")
    private Long cantidadComprada;

    @Column(name = "PRECIO_TOTAL")
    private int precioTotal;


    //bi-directional many-to-one asociado a cliente y producto
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_Cliente")
    private ClienteEntity cliente;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_Producto")
    private ProductoEntity producto;

}
