package com.proyecto.coderhouse.facturacion.modelo.cliente;


import lombok.Data;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Validated
public class ClienteRequest {

    @NotBlank(message = "El Id Cliente es obligatorio")
    private String idCliente;
    @NotBlank(message = "El nombre es obligatorio")
    private String nombre;
    @NotBlank(message = "El apellido es obligatorio")
    private String apellido;
    @NotNull(message = "El dni es obligatorio")
    private Long dni;

}
