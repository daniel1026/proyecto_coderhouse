package com.proyecto.coderhouse.facturacion.modelo.cliente;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClienteResponse {

    private String idCliente;
    private String nombre;
    private String apellido;
    private long dni;

}
