package com.proyecto.coderhouse.facturacion.modelo.producto;

import lombok.Data;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@Validated

public class ProductoRequest {

    @NotNull(message = "El Id del producto es obligatorio")
    private String idProducto;
    @NotNull(message = "El codigo es obligatorio")
    private Long codigo;
    @NotBlank(message = "La descripcion es obligatoria")
    private String descripcion;
    @NotNull(message = "El precio es obligatorio")
    private Float precio;
    @NotNull(message = "El stock es obligatorio")
    private Integer stock;

}
