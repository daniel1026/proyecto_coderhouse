package com.proyecto.coderhouse.facturacion.modelo.producto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductoResponse {

    private String idProducto;
    private Long codigo;
    private String descripcion;
    private Float precio;
    private Integer stock;

}
