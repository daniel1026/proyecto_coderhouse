package com.proyecto.coderhouse.facturacion.modelo.factura;

import com.proyecto.coderhouse.facturacion.modelo.cliente.ClienteRequest;
import com.proyecto.coderhouse.facturacion.modelo.producto.ProductoRequest;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Validated
public class FacturaRequest {

    @NotBlank(message = "El Id Factura es obligatorio")
    private String idFactura;
    @NotNull(message = "La fecha es obligatoria")
    private Date fecha;
    @NotNull(message = "La Serie de factura es obligatoria")
    private Long serieFactura;
    @NotNull(message = "La cantidad comprada es obligatoria")
    private Long cantidadComprada;
    @NotNull(message = "El precio Total es obligatorio")
    private int precioTotal;
}
