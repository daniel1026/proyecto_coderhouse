package com.proyecto.coderhouse.facturacion.modelo.cliente;

import com.proyecto.coderhouse.facturacion.modelo.factura.FacturaEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "CLIENTES")
public class ClienteEntity {

    @Column(name = "ID_CLIENTE")
    private String idCliente;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDO")
    private String apellido;

    @Column(name = "DNI")
    @Id
    private long dni;


}
